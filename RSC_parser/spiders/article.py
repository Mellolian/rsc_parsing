import scrapy, json
from scrapy_splash import SplashFormRequest, SplashRequest
from RSC_parser.items import RscParserItem
from scrapy.loader import ItemLoader

import re
TAG_RE = re.compile(r'<[^>]+>')
def remove_tags(text):
    return TAG_RE.sub('', text)


form_data = {

        'name': 'OB',
        'issueid': 'ob018001',
        'jname': 'Organic+&+Biomolecular+Chemistry',
        'pageno': '1',
        'isarchive': 'False',
        'issnprint': '1477-0520',
        'issnonline': '',
        'iscontentavailable': 'True',
        'publishOnlyVolume': 'False',
        'articlesubmissionurl': 'http://mc.manuscriptcentral.com/ob',
        'latestissueid': 'OB018001',
        'category': "advancearticles",
        'duration': ''
    }



class ArticleSpider(scrapy.Spider):
    name = 'RSC'
    start_urls =[
        'https://pubs.rsc.org/en/journals/journalissues/ob?_escaped_fragment_=issueid%3Dob017048%26type%3Dcurrent%26issnprint%3D1477-0520#!recentarticles&adv'
        ]

    def parse(self, response):

        yield scrapy.FormRequest(url='https://pubs.rsc.org/en/journals/issues',
                                 method='POST',
                                 formdata=form_data,
                                 callback=self.parse_item)


    def parse_item(self, response):

        issueid = response.css("a::attr(data-issueid)").extract_first()

        articles = response.css('div.capsule.capsule--article ')
        for article in articles:
            issue = response.xpath('.//h4/text()[1]').extract_first()
            doi = article.xpath('.//div[1]/span[3]/a/text()').extract_first()

            if doi:
                item_doi = doi[16:]
            else:
                item_doi = None

            name = article.css('h3.capsule__title::text').extract()
            item_name = ''
            for word in name:
                word = word.replace("\n", "")  # remove all '\n' from text
                while "  " in word:  # if 2 space symbols in sting
                    word = word.replace("  ", " ")
                item_name += remove_tags(word)
            print(item_name)

            authors = article.css('div.article__authors.article__author-link::text').extract()
            item_authors = ''
            for author in authors:
                item_authors += author.strip('\n').strip()

            item_journal = article.xpath('.//span[2]/i/strong/text()').extract()
            year = article.xpath('.//span[2]/text()[1]').extract()
            if year:
                item_year = str(year).split(',')[1]
            else:
                item_year = None
            item_volume = article.xpath('.//span[2]/strong/text()').extract_first()
            if issue:
                item_issue = str(issue).split(',')[1].strip().split(' ')[1]
            else:
                item_issue = None

            pages = article.xpath('.//span[2]/text()[2]').extract_first()

            if pages:
                item_pages = pages[2:]
            else:
                item_pages = None

            if doi:
                articleItem = RscParserItem(doi=item_doi, name=item_name, authors=item_authors, journal=item_journal,
                                            year=item_year, volume=item_volume, issue=item_issue, pages=item_pages)
                yield articleItem



        form_data = {
            'name': 'OB',
            'issueid': issueid,
            'jname': 'Organic+&+Biomolecular+Chemistry',
            'pageno': '1',
            'isarchive': 'False',
            'issnprint': '1477-0520',
            'issnonline': '',
            'iscontentavailable': 'True',
            'publishOnlyVolume': 'False',
            'articlesubmissionurl': 'http://mc.manuscriptcentral.com/ob',
            # 'latestissueid': 'OB018001',
            'category': "advancearticles",
            'duration': ''
        }


        yield scrapy.FormRequest(url='https://pubs.rsc.org/en/journals/issues',
                                 method='POST',
                                 formdata=form_data,
                                 callback=self.parse_item)
