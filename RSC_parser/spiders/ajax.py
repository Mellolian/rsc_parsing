import scrapy, json
from scrapy_splash import SplashFormRequest, SplashRequest
from RSC_parser.items import RscParserItem
from scrapy.loader import ItemLoader

form_data = {

        'name': 'OB',
        'issueid': 'ob018001',
        'jname': 'Organic+&+Biomolecular+Chemistry',
        'pageno': '1',
        'isarchive': 'False',
        'issnprint': '1477-0520',
        'issnonline': '',
        'iscontentavailable': 'True',
        'publishOnlyVolume': 'False',
        'articlesubmissionurl': 'http://mc.manuscriptcentral.com/ob',
        'latestissueid': 'OB018001',
        'category': "advancearticles",
        'duration': ''
    }



class AjaxSpider(scrapy.Spider):
    name = 'Ajax'

    start_urls = ['https://pubs.rsc.org/en/journals/issues']

    def parse(self, response):
        yield scrapy.FormRequest(url='https://pubs.rsc.org/en/journals/issues',
                                 method='POST',
                                               formdata=form_data,
                                               callback=self.parse_item)

    def parse_item(self, response):
        print(response.body)
        # quote = QuoteItem()
        # quote["author"] = response.css(".author::text").extract_first()
        # quote["quote"] = response.css(".content::text").extract_first()
        # yield quote